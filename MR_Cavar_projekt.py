import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import tensorflow as tf  
from sklearn import metrics
import pandas as pd
import glob
import os
import pathlib
from tensorflow.keras import models, layers, datasets
from tensorflow.keras.models import Sequential
from keras.layers import Dense,Dropout
import keras
from keras_tuner import RandomSearch
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import MinMaxScaler
from sklearn.preprocessing import OneHotEncoder


pathlib.Path(__file__).parent.absolute()
os.chdir(pathlib.Path(__file__).parent.absolute())

DATASET_PATH = "UCI_HAR_Dataset/"
TRAIN = "train/"
TEST = "test/"
LABELS = pd.read_csv(r'C:\Users\LUKA\Desktop\FERITfaks\2semestar\MekoRacunastvo\zavrsni-projekt\UCI_HAR_Dataset\activity_labels.txt', sep=" ", header=None)
#print(LABELS)

INPUT_SIGNAL_TYPES = [
    "body_acc_x_",
    "body_acc_y_",
    "body_acc_z_",
    "body_gyro_x_",
    "body_gyro_y_",
    "body_gyro_z_",
    "total_acc_x_",
    "total_acc_y_",
    "total_acc_z_"
]

#------------------------PREPROCESSING
DATASET_PATH = "UCI_HAR_Dataset/"
TRAIN = "train/"
TEST = "test/"

#signal_type_path = DATASET_PATH + TRAIN or TEST + "Inertial Signals/" + INPUT_SIGNAL_TYPES + '.txt'
# Load "X" (the neural network's training and testing inputs)
signal_type_path = glob.glob("UCI HAR Dataset\train\Inertial Signals")
def load_X(X_signals_paths):
    X_signals = []
    #signal_type_path = glob.glob("UCI HAR Dataset\train\Inertial Signals")
    #signal_type_path = os.path.join('UCI HAR Dataset', 'Inertial Signals', '*.txt')  
    for signal_type_path in X_signals_paths:
        file = open (signal_type_path, 'r', encoding='utf-8')
        # Read dataset from disk, dealing with text files' syntax
        X_signals.append(
            [np.array(serie, dtype=np.float32) for serie in [
                row.replace('  ', ' ').strip().split(' ') for row in file
            ]]
        )
        file.close()

    return np.transpose(np.array(X_signals), (1, 2, 0))

X_train_signals_paths = [
    DATASET_PATH + TRAIN + "Inertial_Signals/" + signal + "train.txt" for signal in INPUT_SIGNAL_TYPES
]
X_test_signals_paths = [
    DATASET_PATH + TEST + "Inertial_Signals/" + signal + "test.txt" for signal in INPUT_SIGNAL_TYPES
]

X_train = load_X(X_train_signals_paths)
X_test = load_X(X_test_signals_paths)
#print(X_train)
#print(X_test)
#print("------------------------------------")


# Load "y" (the neural network's training and testing outputs)

def load_y(y_path):
    file = open(y_path, 'r')
    # Read dataset from disk, dealing with text file's syntax
    y_ = np.array(
        [elem for elem in [
            row.replace('  ', ' ').strip().split(' ') for row in file
        ]],
        dtype=np.int32
    )
    file.close()

    # Substract 1 to each output class for friendly 0-based indexing
    return y_ - 1 #oduzimanje s jedan radi 0-baznog indeksiranja

y_train_path = DATASET_PATH + TRAIN + "y_train.txt"
y_test_path = DATASET_PATH + TEST + "y_test.txt"

y_train = load_y(y_train_path)
y_test = load_y(y_test_path)
#print(y_test)
#print(y_train)
#----------------------------------------------

'''
X_train, y_train = X_train.iloc[:, :-2], y_train.iloc[:, -1:]
X_test, y_test = X_test.iloc[:, :-2], y_test.iloc[:, -1:]
X_train.shape, y_train.shape

#X_train = OneHotEncoder.fit_transform(X_train).toarray() 
#print("OVOOOOOO",X_train)

le = LabelEncoder()
y_train = le.fit_transform(y_train)
y_test = le.fit_transform(y_test)

X_test = X_test.reshape(-1,1)
y_test = y_test.shape
X_train = X_train.reshape(-1,1)
y_train = y_train.shape
print("x_test",X_test)
print(type(X_test))
print("x_train",X_train)
print(type(X_train))
'''

'''scaling_data = MinMaxScaler()
X_train = scaling_data.fit_transform(X_train)
X_test = scaling_data.transform(X_test)

print("y_test",y_test)
print(type(y_test))
print("y_train",y_train)
print(type(y_train))

#X_test = X_test.reshape(-1,1)
#X_train = X_train.reshape(-1,1)
'''

'''
y_test = np.array(y_test)
y_train = np.array(y_train)
print(y_test.shape)
print(y_train.shape)
print("-------------------------------------------")

X_train = np.delete(X_train, [1])
print(X_train.shape)
X_test = np.delete(X_test, 1)
print(X_test.shape)
'''
def one_hot(y_, n_classes=9):
    # Function to encode neural one-hot output labels from number indexes
    # e.g.:
    # one_hot(y_=[[5], [0], [3]], n_classes=6):
    #     return [[0, 0, 0, 0, 0, 1], [1, 0, 0, 0, 0, 0], [0, 0, 0, 1, 0, 0]]

    y_ = y_.reshape(len(y_))
    return np.eye(n_classes)[np.array(y_, dtype=np.int32)]

y_test = one_hot(y_test)
y_train = one_hot(y_train)

y_test = y_test.reshape(-1,1)
y_train = y_train.reshape(-1,1)

print("---------------------------------")
print(X_train.shape, y_train.shape)
print(X_test.shape, y_test.shape)
print("-------------------------------------------")



print(X_train.shape[-1])
#X_train = np.reshape(X_train, (X_train.shape[0],X_train.shape[1],-1))
#X_test = np.reshape(X_test, (X_test.shape[0],X_test.shape[1],-1))

#X_train = X_train.reshape((7352,128,-1))
X_train = tf.reshape(X_train, [66168,128])
X_test = tf.reshape(X_test, [26523,128])
#num_rows, num_cols = X_train.shape
#input_shape=(1,num_cols)
#train_data = tf.data.Dataset.from_tensor_slices((X_train, y_train))
#valid_data = tf.data.Dataset.from_tensor_slices((X_test, y_test))

#model<------------------------------------------
#ideja da ga malo promijeniš da izgleda kao za rusu



model = Sequential()
model.add(Dense(units=64,kernel_initializer='normal',activation='sigmoid',input_dim=X_train.shape[1]))
model.add(Dropout(0.2))
model.add(Dense(units=6,kernel_initializer='normal',activation='softmax'))
model.compile(optimizer='adam',loss='sparse_categorical_crossentropy',metrics=['accuracy'])
history = model.fit(X_train, y_train, batch_size = 64, epochs= 10, validation_data = (X_test, y_test))

#------------------------------------------------------------
def build_model(hp):
    model = keras.Sequential()
    for i in range(hp.Int('num_layers', 2, 25)):
        model.add(layers.Dense(units = hp.Int('units' + str(i), min_value=32, max_value=512, step=32),
                               kernel_initializer= hp.Choice('initializer', ['uniform', 'normal']),
                               activation= hp.Choice('activation', ['relu', 'sigmoid', 'tanh'])))
    model.add(layers.Dense(6, kernel_initializer= hp.Choice('initializer', ['uniform', 'normal']), activation='softmax'))
    model.add(
            Dropout(0.2))
    model.compile(
        optimizer = 'adam',
        loss='sparse_categorical_crossentropy',
        metrics=['accuracy'])
    return model


tuner = RandomSearch(
    build_model,
    objective='val_accuracy',
    max_trials= 5,
    executions_per_trial=3,
    directory='rezultati', project_name = 'Human_activity_recognition')

tuner.search_space_summary()

tuner.search(X_train, y_train,
             epochs= 10,
             validation_data=(X_test, y_test))

#---------------------------------------------------------------------------------------------
model=tuner.get_best_models(num_models=1)[0]
history = model.fit(X_train,y_train, epochs=51, validation_data=(X_test,y_test))

#----------------------------------------------------------------------------------
model.summary()

Callback = tf.keras.callbacks.EarlyStopping(monitor='accuracy', patience=3)
mo_fitt = model.fit(X_train,y_train, epochs=200, validation_data=(X_test,y_test), callbacks=Callback)

#----------------------------------------------------------------------
accuracy = mo_fitt.history['accuracy']
loss = mo_fitt.history['loss']
validation_loss = mo_fitt.history['val_loss']
validation_accuracy = mo_fitt.history['val_accuracy']


plt.figure(figsize=(15, 7))
plt.subplot(2, 2, 1)
plt.plot(range(5), accuracy, label='Training Accuracy')
plt.plot(range(5), validation_accuracy, label='Validation Accuracy')
plt.legend(loc='upper left')
plt.title('Accuracy : Training Vs Validation ')



plt.subplot(2, 2, 2)
plt.plot(range(5), loss, label='Training Loss')
plt.plot(range(5), validation_loss, label='Validation Loss')
plt.title('Loss : Training Vs Validation ')
plt.legend(loc='upper right')
plt.show()

#----------------------------------------------------------------------------
#konfzijska matrica dodati